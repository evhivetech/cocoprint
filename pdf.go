package cocoprint

import (
	"os"
	"strings"

	wkhtmltopdf "github.com/SebastiaanKlippert/go-wkhtmltopdf"
)

type PDF struct {
	FilePath   string
	FileName   string
	Margin     Margin
	HeaderName string
	HeaderPath string
	Templates  []Template
	Options    Options
	Data       interface{}
}

type Options struct {
	DisableSmartShrinking bool
}

type Template struct {
	Header bool
	Path   string
	Name   string
}

type Margin struct {
	MarginLeft   uint
	MarginTop    uint
	MarginBottom uint
	MarginRight  uint
}

func (p *PDF) SetTemplates(template ...Template) {
	p.Templates = template
	return
}

func (p *PDF) GetTemplate() []Template {
	return p.Templates
}

func CreateHTMLToPDF(pdf PDF) (string, error) {

	var (
		page *wkhtmltopdf.Page
	)

	pdfg, err := wkhtmltopdf.NewPDFGenerator()
	if err != nil {
		return "", err
	}

	// Set global options
	pdfg.Dpi.Set(600)
	pdfg.NoCollate.Set(false)
	pdfg.PageSize.Set(wkhtmltopdf.PageSizeA4)

	if pdf.Margin.MarginLeft > 0 {
		pdfg.MarginLeft.Set(pdf.Margin.MarginLeft)
	}

	if pdf.Margin.MarginTop > 0 {
		pdfg.MarginTop.Set(pdf.Margin.MarginTop)
	}

	if pdf.Margin.MarginRight > 0 {
		pdfg.MarginRight.Set(pdf.Margin.MarginRight)
	}

	if pdf.Margin.MarginBottom > 0 {
		pdfg.MarginBottom.Set(pdf.Margin.MarginBottom)
	}

	templates := pdf.GetTemplate()
	headerPath := pdf.HeaderPath + pdf.HeaderName
	defer remove(headerPath, templates)

	lengthOfTemplate := len(templates)
	for i, template := range templates {
		page = wkhtmltopdf.NewPage(template.Path + template.Name)

		if template.Header && (pdf.HeaderName != "" && pdf.HeaderPath != "") {
			page.PageOptions.HeaderHTML.Set(headerPath)
		}

		if (lengthOfTemplate - 1) > i {
			pdfg.AddPage(page)
		}

		page.DisableSmartShrinking.Set(pdf.Options.DisableSmartShrinking)
	}

	// Set options for this page
	//page.FooterRight.Set("[page]")

	// Add to document
	pdfg.AddPage(page)

	// Create PDF document in internal buffer
	err = pdfg.Create()
	if err != nil {
		return "", err
	}

	// Write buffer contents to file on disk
	fileLocation := pdf.FilePath + pdf.FileName + ".pdf"
	err = pdfg.WriteFile(fileLocation)
	if err != nil {
		return "", err
	}

	return fileLocation, nil
}

func remove(headerPath string, templates []Template) error {

	if !strings.Contains(headerPath, "template") {
		if err := os.RemoveAll(headerPath); err != nil {
			return err
		}
	}

	for _, template := range templates {
		pathTemplate := template.Path + template.Name
		if !strings.Contains(pathTemplate, "template") {
			if err := os.RemoveAll(pathTemplate); err != nil {
				return err
			}
		}
	}

	return nil
}
