package cocoprint

import (
	"fmt"
	"os"
	"testing"
	"time"
)

func setupTestCaseHtml(t *testing.T) func(t *testing.T) {
	t.Log("setup test case ")

	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	path := pwd + "/temp"
	if err := os.MkdirAll(path, 0777); err != nil {
		t.Log("cant create folder")
	}

	return func(t *testing.T) {
		t.Log("teardown test case ")
		os.RemoveAll(path)
	}
}

func Test_cocoprint_CreateTemplateToHTML(t *testing.T) {

	setupTestCase(t)
	//defer teardownTestCase(t)

	pwd, _ := os.Getwd()

	t.Run("generate template to html", func(t *testing.T) {
		pdf := PDF{
			FileName: "lease2",
			FilePath: pwd + "/temp/",
			Margin: Margin{
				MarginLeft:   20,
				MarginTop:    30,
				MarginBottom: 25,
				MarginRight:  20,
			},
			HeaderName: "LA_header.html",
			HeaderPath: pwd + "/template/",
			Templates: []Template{
				Template{Path: pwd + "/template/", Name: "LA_cover.html"},
				Template{Header: true, Path: pwd + "/template/", Name: "LA.html"},
				Template{Header: true, Path: pwd + "/template/", Name: "LA_appendix.html"},
			},
			Data: LeaseAgreementReport{
				ID:                   1,
				BookingID:            2,
				BookingFacilityID:    3,
				FacilityID:           4,
				StartDate:            &[]time.Time{time.Now()}[0],
				EndDate:              &[]time.Time{time.Now()}[0],
				FinalExpDate:         &[]time.Time{time.Now()}[0],
				TerminationDate:      &[]time.Time{time.Now()}[0],
				FinalRent:            1000,
				BaseRent:             1500,
				LeaseAgreementNumber: "PO/2018/06/000001",
				Info: Info{
					LANumber: "",
					Date:     time.Now(),
				},
				Tenant: Tenant{
					Company: Company{
						IncorporationDate: &[]time.Time{time.Now()}[0],
					},
				},
				User: User{
					FirstName: "user a",
				},
				BookingItem: BookingItem{
					Facility: Facility{
						ValidDate: &[]time.Time{time.Now()}[0],
					},
					BookingStartDate:    &[]time.Time{time.Now()}[0],
					BookingExpDate:      &[]time.Time{time.Now()}[0],
					BookingExpDateFinal: &[]time.Time{time.Now()}[0],
				},
				EvHive: EvHive{
					Name:      "Cocowork",
					AddressHQ: "jalan jalan",
				},
			},
		}

		html, err := CreateTemplateToHTML(pdf)
		_, err = CreateHTMLToPDF(html)

		if (err != nil) != false {
			t.Errorf("cocoprint_CreateTemplateToHTML() error = %v, wantErr %v", err, false)
			return
		}

	})
}

func Test_cocoprint_CreateTemplateToHTMLWithoutHeader(t *testing.T) {

	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)

	pwd, _ := os.Getwd()

	t.Run("generate template to html", func(t *testing.T) {
		pdf := PDF{
			FileName: "lease2",
			FilePath: pwd + "/temp/",
			Margin: Margin{
				MarginLeft:   20,
				MarginTop:    30,
				MarginBottom: 25,
				MarginRight:  20,
			},
			Templates: []Template{
				Template{Path: pwd + "/template/", Name: "LA_cover.html"},
			},
			Data: LeaseAgreementReport{
				ID:                   1,
				BookingID:            2,
				BookingFacilityID:    3,
				FacilityID:           4,
				StartDate:            &[]time.Time{time.Now()}[0],
				EndDate:              &[]time.Time{time.Now()}[0],
				FinalExpDate:         &[]time.Time{time.Now()}[0],
				TerminationDate:      &[]time.Time{time.Now()}[0],
				FinalRent:            1000,
				BaseRent:             1500,
				LeaseAgreementNumber: "PO/2018/06/000001",
				Info: Info{
					LANumber: "",
					Date:     time.Now(),
				},
				Tenant: Tenant{
					Company: Company{
						IncorporationDate: &[]time.Time{time.Now()}[0],
					},
				},
				User: User{
					FirstName: "user a",
				},
				BookingItem: BookingItem{
					Facility: Facility{
						ValidDate: &[]time.Time{time.Now()}[0],
					},
					BookingStartDate:    &[]time.Time{time.Now()}[0],
					BookingExpDate:      &[]time.Time{time.Now()}[0],
					BookingExpDateFinal: &[]time.Time{time.Now()}[0],
				},
				EvHive: EvHive{
					Name:      "Cocowork",
					AddressHQ: "jalan jalan",
				},
			},
		}

		html, err := CreateTemplateToHTML(pdf)
		_, err = CreateHTMLToPDF(html)

		if (err != nil) != false {
			t.Errorf("cocoprint_CreateTemplateToHTML() error = %v, wantErr %v", err, false)
			return
		}

	})
}
