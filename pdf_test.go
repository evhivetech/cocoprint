package cocoprint

import (
	"fmt"
	"os"
	"testing"
)

func setupTestCase(t *testing.T) func(t *testing.T) {
	t.Log("setup test case ")

	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	path := pwd + "/temp"
	if err := os.MkdirAll(path, 0777); err != nil {
		t.Log("cant create folder")
	}

	return func(t *testing.T) {
		t.Log("teardown test case ")
		os.RemoveAll(path)
	}
}

func Test_cocoprint_CreateHTMLToPDF(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)

	pwd, _ := os.Getwd()

	t.Run("create a new pdf", func(t *testing.T) {
		pdf := PDF{
			FileName: "test",
			FilePath: pwd + "/temp/",
			Margin: Margin{
				MarginLeft:   20,
				MarginTop:    30,
				MarginBottom: 25,
				MarginRight:  20,
			},
		}
		pdf.SetTemplates(Template{Path: "https://godoc.org/github.com/SebastiaanKlippert/go-wkhtmltopdf"})

		_, err := CreateHTMLToPDF(pdf)

		if (err != nil) != false {
			t.Errorf("cocoprint_CreateHTMLToPDFromTemplate() error = %v, wantErr %v", err, false)
			return
		}

		if _, err := os.Stat("./temp/test.pdf"); os.IsNotExist(err) {
			t.Errorf("test.pdf should exist")
		}
	})
}

func Test_cocoprint_CreateHTMLToPDFromTemplate(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)

	pwd, _ := os.Getwd()

	t.Run("create a new pdf with template", func(t *testing.T) {

		pdf := PDF{
			FileName: "test2",
			FilePath: pwd + "/temp/",
			Margin: Margin{
				MarginLeft:   20,
				MarginTop:    30,
				MarginBottom: 25,
				MarginRight:  20,
			},
			Templates: []Template{
				Template{Path: pwd + "/template/page1.html"},
				Template{Path: pwd + "/template/page2.html"},
			},
		}

		_, err := CreateHTMLToPDF(pdf)

		if (err != nil) != false {
			t.Errorf("cocoprint_CreateHTMLToPDFromTemplate() error = %v, wantErr %v", err, false)
			return
		}

		if _, err := os.Stat("./temp/test2.pdf"); os.IsNotExist(err) {
			t.Errorf("test.pdf should exist")
		}

	})
}

func Test_cocoprint_CreateHTMLToPDFromLeaseAgreementTemplate(t *testing.T) {
	setupTestCase(t)
	//defer teardownTestCase(t)

	pwd, _ := os.Getwd()

	t.Run("create a new pdf with lease agreement template", func(t *testing.T) {

		pdf := PDF{
			FileName: "test2",
			FilePath: pwd + "/temp/",
			Margin: Margin{
				MarginLeft:   20,
				MarginTop:    30,
				MarginBottom: 25,
				MarginRight:  20,
			},
			HeaderPath: pwd + "/template/LA_header.html",
			Templates: []Template{
				Template{Path: pwd + "/template/", Name: "LA_cover.html"},
				Template{Header: true, Path: pwd + "/template/", Name: "LA.html"},
				Template{Header: true, Path: pwd + "/template/", Name: "LA_appendix.html"},
			},
		}

		_, err := CreateHTMLToPDF(pdf)

		if (err != nil) != false {
			t.Errorf("cocoprint_CreateHTMLToPDFromTemplate() error = %v, wantErr %v", err, false)
			return
		}

		if _, err := os.Stat("./temp/test2.pdf"); os.IsNotExist(err) {
			t.Errorf("test.pdf should exist")
		}

	})
}
